import './App.css';
import React, {useEffect, useState} from "react";
import "./main.scss"
import CardsContainer from "./Components/CardsContainer/CardsContainer";
import Header from "./Components/Header/Header";

function App() {
    const cartList = JSON.parse(localStorage.getItem('cart'));
    const [countCart, setCountCart] = useState(0);
    const [cart, setCart] = useState(cartList || []);

    useEffect(() => {
        if (cartList) {
            setCart(cartList);
            setCountCart(cartList.length);
        }
    }, []);

    useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(cart));
        setCountCart(cart.length);
    }, [cart]);

    const addToCart = (product) => {
        const newCart = [...cart, product];
        setCart(newCart);
        setCountCart(newCart.length);
    };

    const favouritesList = JSON.parse(localStorage.getItem("favouritesList"))
    const [favoritesCount, setFavoritesCount] = useState(0);
    const [favorites, setFavorites] = useState(favouritesList || []);

    useEffect(() => {
        if (favouritesList) {
            setFavorites(favouritesList)
            setFavoritesCount(favouritesList.length)
        }
    }, []);

    useEffect(() => {
        localStorage.setItem("favouritesList", JSON.stringify(favorites))
        setFavoritesCount(favorites.length)
    }, [favorites]);


    const addToFavorites = (prod) => {
        if (favorites.includes(prod)) {
            setFavorites(favorites.filter((item) => item !== prod));
            setFavoritesCount(favorites.length)
        } else {
            setFavorites([...favorites, prod]);
            setFavoritesCount(favorites.length)
        }
    };


    return (
        <div className="App">
            <Header title={"Andrusha Clothes"} cartItemsCount={countCart} favoritesCount={favoritesCount}/>
            <CardsContainer addToCart={addToCart} addToFavorites={addToFavorites}/>
        </div>
    );
}

export default App;