import PropTypes from "prop-types";

function Modal({header, closeButton, text, actions, isModalOpen, onClick}) {
    const handleCloseModal = (e) => {
        if (e.target.className === "modal") {
            onClick();
        }
    };

    return (
        isModalOpen && <div onClick={handleCloseModal} className="modal">
            <div className="modal_window">
                <div className="modal_window_header">
                    <h3 className="title">{header}</h3>
                    {closeButton && <span onClick={onClick} className="times">&times;</span>}
                </div>

                <div className="modal_window_main">
                    <p>{text}</p>
                </div>

                <div className="modal_window_footer">{actions}</div>
            </div>
        </div>
    )
}

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    isModalOpen: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired,
    actions: PropTypes.element
}

Modal.defaultProps = {
    closeButton: false,
    text: "Do you want to continue?",
    actions: <div>Close</div>
}

export default Modal