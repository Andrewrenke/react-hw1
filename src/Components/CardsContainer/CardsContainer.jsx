import {useEffect, useState} from "react";
import ProductCard from "../ProductCard/ProductCard";
import "./CardsContainer.scss"
import PropTypes from "prop-types";

function CardsContainer({addToFavorites, addToCart}) {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetch('data.json')
            .then((response) => response.json())
            .then((data) => setProducts(data));
    }, [products]);

    return (
        <div className="cards-container">
            {products.map((prod, index) => {
                return <ProductCard
                    key={index}
                    addToFavorites={addToFavorites}
                    addToCart={addToCart}
                    title={prod.name}
                    price={prod.price}
                    imgUrl={prod.url}
                    color={prod.color}
                    article={prod.article}
                />
            })}
        </div>
    )
}

CardsContainer.propTypes = {
    addToCart: PropTypes.func.isRequired,
    addToFavorites: PropTypes.func.isRequired
}

export default CardsContainer