import "./Header.scss"
import Cart from "./icons/Cart";
import Favourite from "./icons/Favourite";
import PropTypes from "prop-types";

function Header({title, cartItemsCount, favoritesCount}) {
    return (
        <header className="header">
            <h1 className="header_title">{title}</h1>
            <div className="icon-container">
                <Favourite favoritesCount={favoritesCount}/>
                <Cart cartItemsCount={cartItemsCount}/>
            </div>
        </header>
    )
}

Header.propTypes = {
    title: PropTypes.string.isRequired,
    favoritesCount: PropTypes.number.isRequired,
    cartItemsCount: PropTypes.number.isRequired
}
export default Header