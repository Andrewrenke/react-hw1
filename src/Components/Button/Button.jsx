import "./Button.scss"
import PropTypes from "prop-types";

function Button({backgroundColor, text, onClick}) {
    return (
        <button className="button" onClick={onClick} style={{backgroundColor}}>{text}</button>
    )
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}

Button.defaultProps = {
    backgroundColor: "#ccc"
}

export default Button