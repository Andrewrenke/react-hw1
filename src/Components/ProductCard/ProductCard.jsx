import "./ProductCard.scss"
import Button from "../Button/Button";
import ToFavourite from "./icons/ToFavourite";
import React, {useEffect, useState} from "react";
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";

function ProductCard({title, imgUrl, article, color, price, addToCart, addToFavorites}) {
    const [isModalOpen, setIsFirstModalOpen] = useState(false);
    const handleModal = () => setIsFirstModalOpen(!isModalOpen);

    const status = JSON.parse(localStorage.getItem(`isSelected(${article})`))
    const [isSelected, setIsSelected] = useState(status || false)

    useEffect(() => {
            localStorage.setItem(`isSelected(${article})`, JSON.stringify(isSelected))
    }, [isSelected]);

    const toggleSelect = () => {
        setIsSelected(!isSelected)
    }


    return (
        <>
            <div className="product-card">
                <ToFavourite
                    isSelected={isSelected}
                    onClick={() => {
                        addToFavorites(article)
                        toggleSelect()
                    }}/>
                <img className="product-card_image" src={imgUrl} alt="Not Found"/>
                <p className="product-card_title">{title}</p>
                <p className="product-card_color">Color: {color}</p>
                <p className="product-card_article">Article: {article}</p>
                <h3 className="product-card_price">${price}</h3>
                <Button
                    text={"Add to Cart"}
                    onClick={handleModal}
                    backgroundColor={"#000"}/>
            </div>

            <Modal
                onClick={handleModal}
                header={"Thanks for your order!"}
                text={`${title} has been added to your Cart`}
                isModalOpen={isModalOpen}
                actions={
                    <>
                        <Button
                            onClick={handleModal}
                            backgroundColor={"#ccc"}
                            text={"Cancel"}/>
                        <Button
                            onClick={() => {
                                handleModal()
                                addToCart({title, article, price, color})
                            }}
                            backgroundColor={"#ccc"} text={"Ok"}/>
                    </>}
            />
        </>
    )
}

ProductCard.propTypes = {
    title: PropTypes.string.isRequired,
    imgUrl: PropTypes.string.isRequired,
    article: PropTypes.string.isRequired,
    color: PropTypes.string,
}

ProductCard.defaultProps = {
    color: ""
}

export default ProductCard;